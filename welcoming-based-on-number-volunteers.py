"""
DATE: 8/12/2022
Iteration #1

write a function that will take the number of volunteers -> returns a randomly generated assignment based on that number

create a list based on the number of people
shuffle(deck)

Any way to use a dictionary? is that necessary?


output a random assignment of numbers to roles


"""
import random


def welcoming_roles(num_volunteers):
    # return random.randrange(1,num_volunteers)
    roles_dict = {}
    roles_list = ["left_front_usher", "left_back_usher", "right_front_usher", "right_back_usher", "parking", "Info Booth"]
    random.shuffle(roles_list)#will shuffle roles list
    count = 0
    for person in range(0,num_volunteers):
        roles_dict[person] = roles_list[count]
        count += 1
        # for each person, insert them into this dictionary. this will not be random
        # the values will be the different roles - and they will be random
        # how do I randomly assign a role into the value?
    return roles_dict


print(welcoming_roles(6))

