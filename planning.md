# Problem this solves
* Ease the assignement of roles for welcoming team volunteers

# How does it solve this problem?
Randomly generate welcoming roles based on volunteers present

## Tags
Dictionary, random library

# Dreams
* iphone/android app that can do this
* randomization is influenced by the roles people had the week before... this means role assignment needs to be saved and logged

# iteration 1
function takes a number of volunteers, and assigns to each number, a role, that is randomly genrated.
limitations: takes a number of volunteers.
Roles are only 6 total.

# iteration 2
Takes a list of names and will assign to roles
I created a nifty tiny function to generate a list instead of writing one.

LIMITATIONS:
* still only takes 6 roles
* the key is the person.. in a sense, it may have been better to have the role be the key, and not the value. That way I can create a list, and
add multiple people to a role.

Additional logic:
* if 7 people: add a person to info center. 
* if 8 people: add person to parkin

* if 5 people - take out the parking role.

_crew will not work with 4 people, no need to accomodate in app_

# iteration 3

