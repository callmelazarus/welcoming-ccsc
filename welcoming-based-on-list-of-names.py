"""
DATE: 8/12/2022
Iteration #2

write a function that will take a list of names -> returns a randomly generated assignment based on that number

create a list based on the number of people
shuffle(deck)

"""
import random

def welcoming_roles(list_volunteers):  #takes a list of volunteers
    # return random.randrange(1,num_volunteers)
    roles_dict = {}
    roles_list = ["left_front_usher", "left_back_usher", "right_front_usher", "right_back_usher", "parking", "Info Booth"]
    random.shuffle(roles_list)#will shuffle roles list
    count = 0
    for person in range(len(list_volunteers)):
        roles_dict[list_volunteers[person]] = roles_list[count]
        count += 1
        # for each person, insert them into this dictionary. this will not be random
        # the values will be the different roles - and they will be random
        # how do I randomly assign a role into the value?
    
    return roles_dict


#funciton that will take a number, and generate a list of strings (letters) based on that number
def list_generator(number):
    list = []
    for i in range(number):
        list.append(chr(65+i))
    return list

num_volunteers = 6
list_volun = list_generator(num_volunteers) # list of strings, with a, b, c ... as the items

print(welcoming_roles(list_volun))

